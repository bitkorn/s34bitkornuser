<?php

namespace Bitkorn\User\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{

    /**
     * @ORM\Column(type="integer", name="user_id")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="user_login", length=25, unique=true)
     */
    protected $login;

    /**
     * @ORM\Column(type="string", name="user_passwd", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", name="user_email", length=254, unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="integer", name="user_active", type="boolean")
     */
    protected $active;

    /**
     * @ORM\Column(type="integer", name="user_date_register", type="boolean")
     */
    protected $registerDate;
    protected $roles = [];
    protected $roleRoutes = [];

    public function getId()
    {
        return $this->id;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getUsername(): string
    {
        return $this->login;
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    public function getRegisterYear()
    {
        return date('Y', $this->registerDate);
    }

    public function getRegisterMonth()
    {
        return date('m', $this->registerDate);
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function eraseCredentials()
    {
        
    }

    public function getRoleTop()
    {
        if (empty($this->roles) || !is_array($this->roles)) {
            return null;
        }
        return $this->roles[0];
    }

    public function getRoleRoutes(): array
    {
        return $this->roleRoutes;
    }

    public function getRoleRouteTop()
    {
        if (empty($this->roleRoutes) || !is_array($this->roleRoutes)) {
            return null;
        }
        return $this->roleRoutes[0];
    }

    /**
     * 
     * @param int $role
     * @return boolean TRUE if user in role else FALSE
     */
    public function isUserInRole(int $role)
    {
        if (!isset($this->roles)) {
            return false;
        }
        if (in_array($role, $this->roles)) {
            return true;
        }
        return false;
    }

    /**
     * Checks if user has at least $role
     * 
     * @param int $role If $role == 4, there must be only a user-ID
     * @return boolean
     */
    public function isUserInRoleMin($role)
    {
        if (!isset($this->roles) || empty($role)) {
            return FALSE;
        }
        if (in_array(1, $this->roles)) { // 1 ist root
            return TRUE;
        }
        if ($role >= 2 && in_array(2, $this->roles)) {
            return TRUE;
        }
        if ($role >= 3 && in_array(3, $this->roles)) {
            return TRUE;
        }
        if ($role == 4 && !empty($this->userId)) { // aktuell die letzte role
            return TRUE;
        }
        if (in_array($role, $this->roles)) {
            return TRUE;
        }
        return FALSE;
    }

    public function isPrivacyPolicyChecked()
    {
        return $this->privacyPolicyChecked;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    public function setLogin($login)
    {
        $this->login = $login;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setActive($active)
    {
        $this->active = $active;
    }

    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    public function setRoles($roles)
    {
        $this->roles = explode(',', $roles);
        sort($this->roles, SORT_NUMERIC);
    }

    public function setRoleRoutes($roleRoutes)
    {
        $this->roleRoutes = explode(',', $roleRoutes);
    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
                // see section on salt below
                // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
                $this->id,
                $this->username,
                $this->password,
                // see section on salt below
                // $this->salt
                ) = unserialize($serialized, array('allowed_classes' => false));
    }

}
