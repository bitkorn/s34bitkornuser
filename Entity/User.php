<?php

namespace Bitkorn\User\Entity;

use Bitkorn\ToolsStuff\Entity\AbstractEntity;

/**
 * 
 */
class User extends AbstractEntity
{

    /**
     * @var array Array with Key=property; value=db column
     */
    protected $mapping = [
        'id' => 'user_id',
        'login' => 'user_login',
        'password' => 'user_passwd',
        'email' => 'user_email',
        'active' => 'user_active',
        'registerTime' => 'user_registertime',
        'rolesCsv' => 'user_roles_csv',
        'roleRoutesCsv' => 'user_roleroutes_csv',
    ];

    /**
     *
     * @var int
     */
    protected $id;

    /**
     * 
     * @var string
     */
    protected $login;

    /**
     * 
     * @var string
     */
    private $password;

    /**
     * 
     * @var string
     */
    protected $email;

    /**
     * 
     * @var bool
     */
    protected $active;

    /**
     *
     * @var int
     */
    protected $registerTime;

    /**
     * 
     * @var string
     */
    protected $rolesCsv;

    /**
     *
     * @var array 0 indexed user roles
     */
    protected $roles = [];

    /**
     *
     * @var string
     */
    protected $roleRoutesCsv;

    /**
     *
     * @var array
     */
    protected $roleRoutes = [];

    /**
     * Set your own rolesAll if needed.
     * @var array
     */
    protected $rolesAll = [1, 2, 3, 4];

    public function exchangeArray(array $data): bool
    {
        if(!parent::exchangeArray($data)) {
            return false;
        }
        if (!empty($this->rolesCsv)) {
            $this->setRoles(explode(',', $this->rolesCsv));
        }
        if (!empty($this->roleRoutesCsv)) {
            $this->setRoleRoutes(explode(',', $this->roleRoutesCsv));
        }
        return true;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function getRegisterTime(): int
    {
        return $this->registerTime;
    }

    /**
     * 
     * @return array Sorted (highest priority/weight first) 0 indexed user roles.
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * 
     * @return int The role with the highest priority/weight.
     */
    public function getRoleTop(): int
    {
        if (empty($this->roles) || !is_array($this->roles)) {
            return 0;
        }
        return $this->roles[0];
    }

    public function getRoleRoutes(): array
    {
        return $this->roleRoutes;
    }

    public function getRoleRouteTop(): string
    {
        if (empty($this->roleRoutes) || !is_array($this->roleRoutes)) {
            return '';
        }
        return $this->roleRoutes[0];
    }

    /**
     * 
     * @param int $role
     * @return boolean TRUE if user in role else FALSE
     */
    public function isUserInRole(int $role): bool
    {
        if (!isset($this->roles) || empty($role)) {
            return false;
        }
        if (in_array($role, $this->roles)) {
            return true;
        }
        return false;
    }

    /**
     * Checks if user has at least $role
     * 
     * @param int $role
     * @return boolean
     */
    public function isUserInRoleMin(int $role): bool
    {
        if (!isset($this->roles) || empty($role)) {
            return false;
        }
        foreach ($this->rolesAll as $index => $r) {
            if ($r == 1) {
                if (in_array($r, $this->roles)) { // 1 is highest priority/weight
                    return TRUE;
                }
            }
            if ($role >= $r && in_array($r, $this->roles)) {
                return TRUE;
            }
            if (!isset($this->rolesAll[$index + 1])) {
                if (in_array($role, $this->roles)) {
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

    /**
     * Will be value sorted (highest priority/weight first).
     * @param array $roles
     */
    public function setRoles(array $roles)
    {
        $this->roles = $roles;
        sort($this->roles, SORT_NUMERIC);
    }

    /**
     * 
     * @param array $roleRoutes Key=route ID; value=route
     */
    public function setRoleRoutes(array $roleRoutes)
    {
        $this->roleRoutes = $roleRoutes;
    }

    /**
     * Will be value sorted (highest priority/weight first).
     * @param array $rolesAll All posible roles.
     */
    public function setRolesAll(array $rolesAll)
    {
        $this->rolesAll = $rolesAll;
        sort($this->rolesAll, SORT_NUMERIC);
    }

}
