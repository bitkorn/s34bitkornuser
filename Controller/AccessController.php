<?php

namespace Bitkorn\User\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * @author Torsten Brieskorn
 */
class AccessController extends Controller
{

    /**
     * @Route("/bkuser/login", name="bitkorn_user_login")
     */
    public function loginAction()
    {
        $auth = false;
        
        return $this->render('@UbungenoUser/login.html.twig', ['auth' => $auth]);
    }

    /**
     * @Route("/bkuser/logout", name="bitkorn_user_logout")
     */
    public function logoutAction()
    {
        
        return $this->render('@UbungenoUser/logout.html.twig');
    }

}
